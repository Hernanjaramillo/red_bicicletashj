var map = L.map('main_map').setView([-34.6012424,-58.3861497], 13); // con l.map es una funcion que le pasamos e id del vid que va acontener el mapa
//con .setview le indicamos que setee el mapa o lo ubique en las coordenadas sacadas de google maps.
// con tileLayer agregamos la capa de mapa 
L.tileLayer( 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    //propiedad attribution que lo pasamos con un json
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStrretMap</a> contributors'
}).addTo(map);

//para colocar las gotitas de los tres puntos
L.marker([-34.6012424,-58.3861497]).addTo(map);
L.marker([-34.596932,-58.3861287]).addTo(map);
L.marker([-34.599564,-58.3778777]).addTo(map);